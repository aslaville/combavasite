# CombavaSite

* Site vitrine responsive réalisée exclusivement en HTML / CSS et utilisant la technique "mobile first"
* Projet concernant le module IHM (Interface Homme Machine) de 1ère année de DUT Informatique 

## Consignes 

L'objectif était de mettre en avant les évènements de l'entreprise Combava, réalisés en partenariat avec différents magasins, 
tout en respectant la charte graphique de l'entreprise.
Cette entreprise de citronnade fictive, devait coller à une image enfantine, simple et naturelle, 
en reprenant l'aspect des stands de citronnades réalisés par les enfants américains dans les années 50.

